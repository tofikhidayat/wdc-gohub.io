var list = news_list.length / 4;
var list_fixed = list.toFixed(0);


//looping data with (-1)


for (var i = 1; i <= list_fixed - 1; i++) {
    $("#page-parent").clone().appendTo("#page-content");
    $("#page-button").clone().appendTo('#page-list-act')
}

//looping data without -1
for (var i = 1; i <= list_fixed; i++) {
    $(".page-list-parent:nth-child(" + i + ")").attr('id', 'page-parent-' + i);
    $(".page-button:nth-child(" + i + ")").find('button').text(i);
    $(".page-button:nth-child(" + i + ")").find('button').attr('target', '#page-parent-' + i);
}
$('.page-button:nth-child(1)').addClass('active');
$("#page-parent-1").addClass('active');

var right_else = $(".page-button").length - 1;

$(document).on('click', '.page-button button', function(event) {
    

    $(".page-list-parent").css('display', 'none');
    $(".page-button").removeClass('active');
    $(this).closest('.page-button').addClass('active');
    $($(".page-button.active").find('button').attr('target')).css('display', 'block');

    

    if($(document).width() >=769)
    {
        var to_handle=$("#page-content").position().top - 100;
    }
    else if ($(document).width() <= 768) {
        var to_handle=$("#page-content").position().top +100;
    }
    //alert(to_handle)
     $("html,body").animate({
        scrollTop : to_handle
    }, 500); 


    if ($(".page-button.active").index() == 0) {
        $(".button-trek#left").attr('disabled', 'true');
        $(".button-trek#right").removeAttr('disabled');
    } else if ($(".page-button.active").index() == right_else) {
        $(".button-trek#left").removeAttr('disabled');
        $(".button-trek#right").attr('disabled', 'true');
    } else {
        $(".button-trek#left").removeAttr('disabled');
        $(".button-trek#right").removeAttr('disabled');
    }

});


$(document).on('click', '.button-trek', function(event) {
    var track_left = $(".page-button.active").index();
    var track_right = $(".page-button.active").index() + 2;
    $('.page-button').removeClass('active');
    $(".page-list-parent").css('display', 'none');
    if ($(this).attr('id') == "left") {

        $(".page-button:nth-child(" + track_left + ")").addClass('active');
        $(".button-trek#right").removeAttr('disabled');

        if($(document).width() >=769)
    {
        var to_handle=$("#page-content").position().top - 100;
    }
    else if ($(document).width() <= 768) {
        var to_handle=$("#page-content").position().top +100;
    }
    //alert(to_handle)
     $("html,body").animate({
        scrollTop : to_handle
    }, 500); 

        if ($(".page-button.active").index() == 0) {
            $(".button-trek#left").attr('disabled', 'true');

        }
    } else if ($(this).attr('id') == "right")
     {
        $(".page-button:nth-child(" + track_right + ")").addClass('active')

        $(".button-trek#left").removeAttr('disabled');
        if($(document).width() >=769)
    {
        var to_handle=$("#page-content").position().top - 100;
    }
    else if ($(document).width() <= 768) {
        var to_handle=$("#page-content").position().top +100;
    }
    //alert(to_handle)
     $("html,body").animate({
        scrollTop : to_handle
    }, 500); 
        if ($(".page-button.active").index() == right_else) {

            $(".button-trek#right").attr('disabled', 'true');
        }



    }

    $($(".page-button.active").find('button').attr('target')).css('display', 'block');


});

$('.ci input').click(function(event) {
  $(".autocomplete-suggestions").removeClass('p2').addClass('p1');
});
$('.search-news').click(function(event) {
  $(".autocomplete-suggestions").removeClass('p1').addClass('p2');
});





jQuery(document).ready(function($) {
    $('.search-news').autoComplete({
                minChars: 1,
                source: function(term, suggest){
                    term = term.toLowerCase();
                    var choices = ['ActionScript', 'AppleScript', 'Asp', 'Assembly', 'BASIC', 'Batch', 'C', 'C++', 'CSS', 'Clojure', 'COBOL', 'ColdFusion', 'Erlang', 'Fortran', 'Groovy', 'Haskell', 'HTML', 'Java', 'JavaScript', 'Lisp', 'Perl', 'PHP', 'PowerShell', 'Python', 'Ruby', 'Scala', 'Scheme', 'SQL', 'TeX', 'XML'];
                    var suggestions = [];
                    for (i=0;i<= news_list.length;i++)
                        if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(news_list[i]["nama"]);
                    suggest(suggestions);
                }
            });
});

