$(document).ready(function() {
	var getDestinationURL = function getDestinationURL(urlParam) {
		var DestinationURL = decodeURIComponent(window.location.search.substring(1)),
			URLvar = DestinationURL.split('&'),
			destinationName, i;
		for (i = 0; i < URLvar.length; i++) {
			destinationName = URLvar[i].split('=');
			if (destinationName[0] === urlParam) {
				return destinationName[1] === undefined ? true : destinationName[1];
			}
		}
	};
	var lokasi_url = getDestinationURL('lokasi');
	var single_data = destinasi.reduce(function(singleData, destinasi) {
		if (destinasi.url == lokasi_url) {
			return singleData.concat(destinasi);
		} else {
			return singleData
		}
	}, []);
	$("title").append(single_data[0].title);
	$("#title-dest").append(single_data[0].nama);
	$("#image").attr('src', 'assets/image/destinations/' + single_data[0].gambar);
	$("#description").append(single_data[0].deskripsi);
	$("#location").append(single_data[0].lokasi);
	$("#credit").append(single_data[0].credit);
	$(".sub-rate").attr('rating', single_data[0].rate);
});